<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

require("Connexion.php");

if(isset($_GET["isbn"]) && $_GET["isbn"]!= ""){
    $c = new Connexion("livres");
    $livre = $c->getLivreParISBN($_GET["isbn"]);
    echo json_encode($livre);
}else if(isset($_GET["action"]) && $_GET["action"]== "all"){
    $c = new Connexion("livres");
    $livres = $c->getAllLivres();
    echo "{livres:".json_encode($livres)."}";
}else if(isset($_GET["action"]) && $_GET["action"]== "dispo"){
    $c = new Connexion("livres");
    $livres = $c->getAllLivresDispo();
    echo "{livres:".json_encode($livres)."}";
}else{
    echo "{result:noresponse}";
}

?>