<?php
class Livre implements JsonSerializable{
    private $id;
    private $titre;
    private $auteur;
    private $editeur;
    private $format;
    private $section;
    private $categorie;
    private $exemplaires = array();

    function __construct($_id, $_titre, $_auteur, $_editeur, $_format, $_section, $_categorie, $_exemplaires) {
        $this->id=$_id;
        $this->titre=$_titre;
        $this->auteur=$_auteur;
        $this->editeur=$_editeur;
        $this->format=$_format;
        $this->section=$_section;
        $this->categorie=$_categorie;
        $this->exemplaires=$_exemplaires;
    }

    public function jsonSerialize (){
        return [
            "isbn" => $this->id,
            "titre" => $this->titre,
            "auteur" => $this->auteur,
            "editeur" => $this->editeur,
            "format" => $this->format,
            "section" => $this->section,
            "categorie" => $this->categorie,
            "exemplaires" => $this->exemplaires
        ];
    }

}