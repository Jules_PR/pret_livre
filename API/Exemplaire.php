<?php
class Exemplaire  implements JsonSerializable{
    private $id;
    private $statut;
    private $proprietaire;
    private $livre;

    function __construct( $_id, $_statut, $_livre, $_proprietaire){
        $this->id = $_id;
        $this->statut = $_statut;
        $this->proprietaire = $_proprietaire;
        $this->livre = $_livre;
    }

    public function jsonSerialize (){
        return [
            "id" => $this->id,
            "statut" => $this->statut,
            "proprietaire" => $this->proprietaire,
            "livre" => $this->livre
        ];
    }
}