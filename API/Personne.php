<?php
class Personne  implements JsonSerializable{
    private $nom;
    private $prenom;
    private $mail;
    private $pass;

    function __construct($_nom, $_prenom, $_mail, $_pass){
        $this->prenom = $_nom;
        $this->id = $_prenom;
        $this->mail = $_mail;
        $this->pass = $_pass;
    }

    public function jsonSerialize (){
        return [
            "nom" => $this->nom,
            "prenom" => $this->prenom,
            "mail" => $this->mail,
            "pass" => $this->pass,
        ];
    }
}