<?php
require("Livre.php");
require("Auteur.php");
require("Personne.php");
require("Exemplaire.php");


class Connexion{
    private $dbh;
function __construct($bd){
    //connexion a la bdd
    try {
        $this->dbh = new PDO('mysql:host=localhost;dbname='.$bd.';charset=utf8', "root", "");
        $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

function getLivreParISBN($id){
    $stmt = $this->dbh->prepare('SELECT * FROM livre WHERE isbn=:id');
    $stmt->bindParam(':id', $id);
    $stmt->execute();
    $row = $stmt->fetch();
    
    //récupération du livre
    $a = new Auteur($row['nom'],$row['prenom']);
    //récupération des exemplaires de ce livre
    $stmt2 = $this->dbh->prepare('SELECT * FROM exemplaire e INNER JOIN personne p ON e.Id_Personne = p.Id_Personne WHERE isbn=:id');
    $stmt2->bindParam(':id', $id);
    $stmt2->execute();
    $exemplaires = array();
    while ($exemplaire = $stmt2->fetch()) { //pour chaque livre
        //le proprio de l'exemplaire
        $p = new Personne($exemplaire['nomPersonne'],$exemplaire['prenomPersonne'],$exemplaire['mailPersonne'],$exemplaire['passPersonne']);
        //récupération des exemplaires de ce livre
        array_push($exemplaires, new Exemplaire($exemplaire['Id_Exemplaire'],$exemplaire['statut'],null,$p));
    }
    return new Livre($row['isbn'], $row['titre'], $a, $row['editeur'], $row['format'], $row['section'], $row['categorie'], $exemplaires);  
}

function getAllLivresDispo(){
        $stmt = $this->dbh->prepare('SELECT  titre , LIVRE.isbn FROM LIVRE 
                                    INNER JOIN Exemplaire ON LIVRE.isbn= Exemplaire.ISBN
                                    WHERE statut= "disponible"
                                    GROUP by isbn
                                    ORDER BY categorie, titre;');
    $stmt->execute();
    $livresPOO = array();
    while ($row = $stmt->fetch()) { //pour chaque livre
        array_push($livresPOO, $this->getLivreParISBN($row['isbn']));
    }
    return $livresPOO;
}

function getAllLivres(){
    $stmt = $this->dbh->prepare('SELECT * FROM LIVRE;');
$stmt->execute();
$livresPOO = array();
while ($row = $stmt->fetch()) { //pour chaque livre
    array_push($livresPOO, $this->getLivreParISBN($row['isbn']));
}
return $livresPOO;
}
}