package com.example.librairie2.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Exemplaire implements Parcelable {
    private String id;
    private String statut;
    private Personne propriétaire;

    public Exemplaire(String id, String statut, Personne propriétaire) {
        this.id = id;
        this.statut = statut;
        this.propriétaire = propriétaire;
    }

    public String getId() {
        return id;
    }

    public String getStatut() {
        return statut;
    }

    public Personne getPropriétaire() {
        return propriétaire;
    }

    ////pour les rendre parcelable
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(statut);
    }

    public static final Parcelable.Creator<Exemplaire> CREATOR
            = new Parcelable.Creator<Exemplaire>() {
        public Exemplaire createFromParcel(Parcel in) {
            return new Exemplaire(in);
        }

        public Exemplaire[] newArray(int size) {
            return new Exemplaire[size];
        }
    };

    private Exemplaire(Parcel in) {
        id = in.readString();
        statut = in.readString();
    }
}
