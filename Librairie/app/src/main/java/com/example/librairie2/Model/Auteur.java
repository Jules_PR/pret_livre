package com.example.librairie2.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Auteur implements Parcelable {
    private String nom;
    private String prenom;

    public Auteur(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    @Override
    ////pour les rendre parcelable
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(nom);
        out.writeString(prenom);
    }

    public static final Parcelable.Creator<Auteur> CREATOR
            = new Parcelable.Creator<Auteur>() {
        public Auteur createFromParcel(Parcel in) {
            return new Auteur(in);
        }

        public Auteur[] newArray(int size) {
            return new Auteur[size];
        }
    };

    private Auteur(Parcel in) {
        nom = in.readString();
        prenom = in.readString();
    }
}
