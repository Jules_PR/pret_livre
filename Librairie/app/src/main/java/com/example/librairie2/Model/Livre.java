package com.example.librairie2.Model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;

public class Livre implements Parcelable {
    private String id;
    private String titre;
    private String format;
    private String section;
    private String categorie;
    private Auteur auteur;
    private String editeur;
    private ArrayList<Exemplaire> listeExemplaires;

    public Livre(String id, String titre, String format, String section, String categorie, Auteur auteur, String editeur, ArrayList<Exemplaire> listeExemplaires) {
        this.id = id;
        this.titre = titre;
        this.format = format;
        this.section = section;
        this.categorie = categorie;
        this.auteur = auteur;
        this.editeur = editeur;
        this.listeExemplaires = listeExemplaires;
    }


    public String getId() {
        return id;
    }

    public String getTitre() {
        return titre;
    }

    public String getFormat() {
        return format;
    }

    public String getCategorie() {
        return categorie;
    }

    public String getEditeur() {
        return editeur;
    }

    public ArrayList<Exemplaire> getListeExemplaires() {
        return listeExemplaires;
    }

    public Auteur getAuteur() {
        return auteur;
    }

    public String getSection() {
        return section;
    }


    ////pour les rendre parcelable
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(id);
        out.writeString(titre);
        out.writeString(format);
        out.writeString(section);
        out.writeString(categorie);
        out.writeString(editeur);
    }

    public static final Parcelable.Creator<Livre> CREATOR
            = new Parcelable.Creator<Livre>() {
        public Livre createFromParcel(Parcel in) {
            return new Livre(in);
        }

        public Livre[] newArray(int size) {
            return new Livre[size];
        }
    };

    private Livre(Parcel in) {
        id = in.readString();
        titre = in.readString();
        format = in.readString();
        section = in.readString();
        categorie = in.readString();
        editeur = in.readString();
    }
}
