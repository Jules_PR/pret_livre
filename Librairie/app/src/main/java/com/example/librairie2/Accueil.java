package com.example.librairie2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.librairie2.Model.Auteur;
import com.example.librairie2.Model.Exemplaire;
import com.example.librairie2.Model.Livre;
import com.example.librairie2.Model.MySingleton;
import com.example.librairie2.Model.Personne;
import com.google.android.material.navigation.NavigationBarView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;

public class Accueil extends AppCompatActivity {

    private ArrayList<Livre> lesLivres = new ArrayList();
    private ArrayList<Livre> tousLesLivres = new ArrayList();

    private EditText rechercheInput;
    private TextView nbLivres;

    private LivreAdapter adapter;
    private RecyclerView recyclerView;

    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);

        //Récupérer tous les livres
        recupererLesLivresHTTP();

        //bar de recherche
        rechercheInput = findViewById(R.id.recherche);
        rechercheInput.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String recherche = rechercheInput.getText().toString();
                lesLivres.clear();
                for (int i=0; i<tousLesLivres.size(); i++){
                    Livre currentLivre = tousLesLivres.get(i);
                    if(currentLivre.getTitre().contains(recherche.toLowerCase(Locale.ROOT)) || currentLivre.getTitre().contains(recherche.toUpperCase(Locale.ROOT)) || currentLivre.getTitre().contains(recherche)){
                        lesLivres.add(currentLivre);
                    }
                }
                adapter.notifyDataSetChanged();
                if(lesLivres.isEmpty()) {
                    nbLivres.setText("Aucun livre trouvé");
                }else if (lesLivres.size() == tousLesLivres.size()){
                    nbLivres.setText("");
                }else{
                    nbLivres.setText(lesLivres.size() + " livres trouvés");
                }
            }
        });
        nbLivres = findViewById(R.id.nbLivres);

        //spinner
        spinner = (Spinner) findViewById(R.id.select);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this, R.array.planets_array, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String choix = spinner.getItemAtPosition(i).toString();
                lesLivres.clear();

                if(choix == "Tout") {
                    lesLivres = tousLesLivres;
                    adapter.notifyDataSetChanged();
                }
                else if(choix == "Jeunesse") {
                    for (int n = 0; n < tousLesLivres.size(); n++) {
                        Livre currentLivre = tousLesLivres.get(n);
                        if (currentLivre.getSection() == "Jeunesse") {
                            lesLivres.add(currentLivre);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
                else if(choix == "Adulte") {
                    for (int n = 0; n < tousLesLivres.size(); n++) {
                        Livre currentLivre = tousLesLivres.get(n);
                        if (currentLivre.getSection() == "Adulte") {
                            lesLivres.add(currentLivre);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        // set up the RecyclerView
        recyclerView = findViewById(R.id.listeLivres);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new LivreAdapter(this, lesLivres);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        //ouverture page détail
                        Intent intent = new Intent(getApplicationContext(), DetailLivre.class);
                        Livre LivreCliqué = lesLivres.get(position);
                        intent.putExtra("leLivre", LivreCliqué);
                        intent.putExtra("lauteur", LivreCliqué.getAuteur());
                        ArrayList<Exemplaire> lesExemplaires = LivreCliqué.getListeExemplaires();
                        intent.putExtra("indexExemplaire", LivreCliqué.getListeExemplaires().size());

                        for (int i = 0; i<lesExemplaires.size(); i++){
                            intent.putExtra("id" +  i, lesExemplaires.get(i).getId());
                            intent.putExtra("statut" +  i, lesExemplaires.get(i).getStatut());
                            intent.putExtra("nom" + i, lesExemplaires.get(i).getPropriétaire().getNom());
                            intent.putExtra("prenom" + i, lesExemplaires.get(i).getPropriétaire().getPrenom());
                        }
                        startActivity(intent);
                    }
                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
    }

    private void recupererLesLivresHTTP() {
        //String url = "http://192.168.39.197:8080/APIlibraire/?action=all";
        String url = "http://192.168.1.17:8080/APIlibraire/?action=all";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            //Get current json object<vw
                            JSONArray livres = response.getJSONArray("livres");

                            for (int i = 0; i < livres.length(); i++) {
                                JSONObject livre = livres.getJSONObject(i);
                                //Get the current json object data
                                String isbn = livre.getString("isbn");
                                String titre = livre.getString("titre");
                                String editeur = livre.getString("editeur");
                                String format = livre.getString("format");
                                String section = livre.getString("section");
                                String categorie = livre.getString("categorie");
                                Auteur auteur = new Auteur(livre.getJSONObject("auteur").getString("nom"), livre.getJSONObject("auteur").getString("prenom"));
                                //récupération des exemplaires
                                JSONArray lesExemplaires = livre.getJSONArray("exemplaires");
                                ArrayList<Exemplaire> collectionExemplaires = new ArrayList();
                                for (int j = 0; j < lesExemplaires.length(); j++) {
                                    JSONObject exemplaire = lesExemplaires.getJSONObject(j);
                                    Personne propriétaire = new Personne(exemplaire.getJSONObject("proprietaire").getString("nom"), exemplaire.getJSONObject("proprietaire").getString("prenom"));
                                    collectionExemplaires.add(new Exemplaire(exemplaire.getString("id"), exemplaire.getString("statut"), propriétaire));
                                }
                                //Afficher les données sur textView
                                lesLivres.add(new Livre(isbn,titre,format,categorie,section,auteur,editeur,collectionExemplaires));
                                tousLesLivres.add(new Livre(isbn,titre,format,categorie,section,auteur,editeur,collectionExemplaires));
                            }
                            //prévenir l'adapteur que les données ont changé
                            adapter.notifyDataSetChanged();
                        } catch (Exception e) {}
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                Log.e("",error.toString());
                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

}