package com.example.librairie2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.librairie2.Model.Auteur;
import com.example.librairie2.Model.Exemplaire;
import com.example.librairie2.Model.Livre;
import com.example.librairie2.Model.MySingleton;
import com.example.librairie2.Model.Personne;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailLivre extends AppCompatActivity {

    private TextView viewISBN;
    private TextView viewTitre;
    private TextView viewAuteur;
    private TextView viewEditeur;
    private TextView viewCategorie;
    private RecyclerView recyclerView;
    private TextView viewExemplairesLBL;
    private ExemplaireAdapter adapter;
    private Button retourButton;

    private Livre livre;
    private Auteur auteur;
    private ArrayList<Exemplaire> lesExemplaires = new ArrayList();
    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_livre);
        Intent intent = getIntent();
        livre = (Livre)intent.getParcelableExtra("leLivre");
        auteur = (Auteur)intent.getParcelableExtra("lauteur");
        index = (Integer)intent.getIntExtra("indexExemplaire",0);

        for (int i = 0; i<index; i++){
            Personne propriétaire = new Personne(intent.getStringExtra("nom" + i), intent.getStringExtra("prenom" + i));
            Exemplaire exemplaire = new Exemplaire(intent.getStringExtra("id" + i), intent.getStringExtra("statut" + i), propriétaire);
            lesExemplaires.add(exemplaire);
        }

        recyclerView = findViewById(R.id.listeExemplaires);
        viewISBN = findViewById(R.id.isbn);
        viewTitre = findViewById(R.id.titre);
        viewAuteur = findViewById(R.id.auteur);
        viewEditeur = findViewById(R.id.editeur);
        viewCategorie = findViewById((R.id.categorie));
        viewExemplairesLBL = findViewById(R.id.exemplaireLabel);

        retourButton = findViewById(R.id.retourButton);
        retourButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Accueil.class);
                startActivity(intent);
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ExemplaireAdapter(this, lesExemplaires);
        recyclerView.setAdapter(adapter);
       // recupererLesDetailHTTP(isbn);

        viewTitre.setText(livre.getTitre());
        viewISBN.setText(livre.getId());
        if(auteur.getNom().isEmpty() || auteur.getPrenom().isEmpty()) {
            viewAuteur.setText("Auteur inconnu");
        }else{
            viewAuteur.setText("De : " + auteur.getPrenom() + " " + auteur.getNom());
        }
        viewEditeur.setText("Editeur : " + livre.getEditeur());
        viewCategorie.setText("Categorie : " + livre.getCategorie());
        //A FAIRE : EXEMPLAIRES PARCELABLES
        if (lesExemplaires.isEmpty()){
            viewExemplairesLBL.setText("Il n'y a pas d'exemplaires empruntés");
        }
    }
}
