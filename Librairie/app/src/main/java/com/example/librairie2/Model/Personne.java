package com.example.librairie2.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Personne implements Parcelable {
    private String nom;
    private String prenom;
    private String mail;
    private String pass;

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getMail() {
        return mail;
    }

    public String getPass() {
        return pass;
    }

    ////pour les rendre parcelable
    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(nom);
        out.writeString(prenom);
    }

    public static final Parcelable.Creator<Personne> CREATOR
            = new Parcelable.Creator<Personne>() {
        public Personne createFromParcel(Parcel in) {
            return new Personne(in);
        }

        public Personne[] newArray(int size) {
            return new Personne[size];
        }
    };

    private Personne(Parcel in) {
        nom = in.readString();
        prenom = in.readString();
    }
}
