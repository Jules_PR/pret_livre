# Documentation technique
## Application librairie

### <ins>Contexte</ins>
>C'est une application mobile Android permettant de faire du prêt de livres entre particuliers. L’idée est que chaque utilisateur possède sa propre librairie personnelle qu’il souhaite partager.

### <ins>Fonctionnalités</ins>

>* L'application affiche tous les livres disponibles sur une liste, si l'utilisateur selectionne un livre
alors il sera dirigé sur une page d'informations du livre en question
>* L'application a une page de recherche de livre afin que l'utilisateur trouve le livre qui l'intéresse plus facilement

### <ins>Aperçus</ins>

>**INSERER LES ECRANS DE L'APPLI**

### <ins>Shema d'architecture applicative</ins>

>L'application mobile se connecte à une API et lui envoie des requetes HTTP.  
>L'API va se connecter au serveur BDD et va renvoyer les données demandés à l'application en format JSON, lisible pour l'app  

>![](architecture.PNG)

### <ins>Diagrammes</ins>

>#### Diagramme des cas d'utilisations
>![](useCaseDG.PNG)
  
>#### Diagramme de classe
>![](ClassDG.PNG)

>#### Diagramme de séquence
>

### <ins>Ce qu'il reste à coder</ins>

>* Application mobile
>   * Design de l'application
>   * Composants permettant une meilleure gestions des donnnées